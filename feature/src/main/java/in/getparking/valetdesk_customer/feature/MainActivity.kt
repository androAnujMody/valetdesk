package `in`.getparking.valetdesk_customer.feature


import android.app.Dialog
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.TextUtils
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.Toast
import com.google.gson.JsonObject
import com.koushikdutta.ion.Ion
import kotlinx.android.synthetic.main.activity_main_new.*
import kotlinx.android.synthetic.main.enter_details.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.concurrent.timerTask

class MainActivity : AppCompatActivity() {

    var reqId: String? = null
    var delay = 0 // delay for 0 sec.
    var period = 10000 // repeat every 10 sec.
    private var timer = Timer()
    var isStarted = false
    var number = ""
    var vehNumber = ""
    var list = mutableListOf<String>()
    var inTime: Long = 0
    var vehNum = ""
    var dateFormatter = SimpleDateFormat("hh:mm a")


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_new)
        init()
    }

    private fun init() {
        showDialog()
//        ll_get_my_car.setOnClickListener {
//            //callReqMyCarWS()
//        }
    }

    private fun startTimer() {

        isStarted = true
        timer.scheduleAtFixedRate(timerTask {
            callExistStatusWS()
        }, 0, 10000)
    }


    private fun callGetCarListWS() {
        progress.visibility = View.VISIBLE

        Ion.with(this).load("https://us-central1-getparking-development.cloudfunctions.net/getParkedVehicles?ownerContactNumber=$number").asJsonObject().setCallback { e, result ->
            if (e == null) {
                if (result.get("statusCode").toString() == "201") {
                    parseJson(result)
                } else {
                    Toast.makeText(this, "Error", Toast.LENGTH_SHORT).show()
                    progress.visibility = View.GONE
                }
            } else {
                Toast.makeText(this, "Something went wrong, please try again later", Toast.LENGTH_SHORT).show()
                progress.visibility = View.GONE
            }

        }
    }

    private fun parseJson(result: JsonObject?) {

        var carRequested = false
        var status = ""
        for (i in result?.getAsJsonArray("data")!!) {
            list.add(i.asJsonObject.get("vehicleNumber").asString)
            vehNum = i.asJsonObject.get("vehicleNumber").asString
            inTime = i.asJsonObject.get("vehicleInTimeStamp").asLong
            status = i.asJsonObject.get("status").asString
            carRequested = i.asJsonObject.get("carRequested").asBoolean
        }

        if (list.size > 0) {
            /*rv_list.layoutManager = LinearLayoutManager(this)
            rv_list.adapter = CarAdapter(this, list) {
                callReqMyCarWS(it)
            }*/
        }

        if (TextUtils.isEmpty(vehNum)) {
            error.visibility = View.VISIBLE
            button2.visibility = View.GONE
        } else {
            button2.visibility = View.VISIBLE
            error.visibility = View.GONE
            textView3.visibility = View.VISIBLE
            imageView2.visibility = View.VISIBLE
            iv_left.visibility = View.GONE
            iv_right.visibility = View.GONE
            tv_time.visibility = View.VISIBLE
            textView4.visibility = View.VISIBLE
            textView4.text = "Veh Number : $vehNum"
            tv_time.text = "In Time : ${dateFormatter.format(Date(inTime))}"

            if (carRequested) {
                button2.text = "Your car has already been requested"
                button2.isEnabled = false
            } else {
                if (status.toLowerCase() == "out") {
                    button2.isEnabled = false
                    //Your car was not found by our system. Please contact the ValetDesk
                    button2.text = "Your car was not found by our system. Please contact the ValetDesk"
                } else {
                    button2.isEnabled = true
                }

            }

            button2.setOnClickListener {
                callReqMyCarWS(vehNum)
            }
        }



        progress.visibility = View.GONE
    }

    private fun callReqMyCarWS(carNum: String) {
        progress.visibility = View.VISIBLE

        Ion.with(this).load("https://us-central1-getparking-development.cloudfunctions.net/requestToVehicleExit?ownerContactNumber=$number&vehicleNumber=$carNum").asJsonObject().setCallback { e, result ->
            if (e == null) {
                if (result.get("statusCode").toString() == "201") {
                    reqId = result.getAsJsonObject("data").get("requestId").toString()
                    callExistStatusWS()
                } else {
                    Toast.makeText(this, "Error", Toast.LENGTH_SHORT).show()
                    progress.visibility = View.GONE
                }
            } else {
                Toast.makeText(this, "Something went wrong, please try again later", Toast.LENGTH_SHORT).show()
                progress.visibility = View.GONE
            }
//            Logger.getLogger("Valet").severe("")

        }
    }

    private fun showDialog() {
        var dialog = Dialog(this)
        dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog?.setContentView(R.layout.enter_details)
        dialog.bt_proceed.setOnClickListener {
            if (TextUtils.isEmpty(dialog.et_number.text)) {
                Toast.makeText(this, "Phone Number is compulsory", Toast.LENGTH_SHORT).show()
            } else {

                if (dialog.et_number.text.length < 10) {
                    Toast.makeText(this, "Invalid phone number", Toast.LENGTH_SHORT).show()
                    return@setOnClickListener
                }

                number = dialog.et_number.text.toString()
                vehNumber = dialog.et_veh_num.text.toString()
                callGetCarListWS()
                dialog.dismiss()
            }
        }
        dialog?.show()
        dialog?.window?.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
    }


    private var count: Int = -20

    private fun callExistStatusWS() {
        runOnUiThread {
            progress.visibility = View.VISIBLE
            tv_wait.visibility = View.VISIBLE

            //rv_list.visibility = View.GONE

        }
        Ion.with(this).load("https://us-central1-getparking-development.cloudfunctions.net/getCarExitStatus?requestId=$reqId").asJsonObject().setCallback { e, result ->
            if (e == null) {
                if (result.get("statusCode").toString() == "201") {
                    runOnUiThread {
                        /*ll_status.visibility = View.VISIBLE
                        tv_status.text = result.getAsJsonObject("data").get("status").toString().replace("${""}", "")*/
                        button2.visibility = View.GONE
                        progress_status.visibility = View.VISIBLE
                        count += 10
                        progress_status.progress = count

                        if (count >= 100) {
                            //progress_status.setBackgroundColor(ContextCompat.getColor(this, R.color.colorAccent))
                            progress_status.visibility = View.GONE
                            tv_wait.visibility = View.GONE
                            success.visibility = View.VISIBLE
                            timer.cancel()
                        }

                    }

                    if (!isStarted) {
                        startTimer()
                    }
                } else {
                    runOnUiThread {
                        Toast.makeText(this, "Error", Toast.LENGTH_SHORT).show()
                        progress.visibility = View.GONE
                    }

                }
            } else {

                runOnUiThread {
                    progress.visibility = View.GONE
                    Toast.makeText(this, "Something went wrong, please try again later", Toast.LENGTH_SHORT).show()
                }

            }
//            Logger.getLogger("Valet").severe("")

            runOnUiThread {
                progress.visibility = View.GONE
            }

        }

    }
}
