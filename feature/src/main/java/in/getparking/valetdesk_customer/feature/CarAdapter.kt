package `in`.getparking.valetdesk_customer.feature

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.single_item.view.*

class CarAdapter(private val context: Context, private val list: MutableList<String>, private val onClick: (String) -> Unit) : RecyclerView.Adapter<CarAdapter.CarVH>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CarVH {
        return CarVH(LayoutInflater.from(context).inflate(R.layout.single_item, parent, false), onClick)
    }

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(holder: CarVH, position: Int) {

        holder.initData(list[position])
    }


    class CarVH(itemView: View?, private val onClick: (String) -> Unit) : RecyclerView.ViewHolder(itemView!!) {



        fun initData(data: String) = with(data) {
            itemView.textView2.text = data
            itemView.button.setOnClickListener {
                onClick(data)
            }
        }




    }

}