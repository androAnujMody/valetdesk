package `in`.getparking.valetdesk_customer.feature

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.TextUtils
import android.widget.Toast
import kotlinx.android.synthetic.main.enter_details.*

class EnterDetails : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.enter_details)
        init()
    }

    private fun init() {

        bt_proceed.setOnClickListener {
            if (TextUtils.isEmpty(et_number.text) || TextUtils.isEmpty(et_veh_num.text)) {
                Toast.makeText(this, "Phone Number and Vehicle Number are compulsory", Toast.LENGTH_SHORT).show()
            } else {
                startActivity(Intent(this, MainActivity::class.java))
            }
        }
    }
}