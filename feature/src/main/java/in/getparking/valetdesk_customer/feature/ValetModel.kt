package `in`.getparking.valetdesk_customer.feature

class ValetModel{
    private var valetParkingId: String? = null
    private var vehicleNumber: String? = null
    private var pickUpDriver: String? = null
    private var dropOffDriver: String? = null
    private var vehicleInTimeStamp: Long? = null
    private var vehicleOutTimeStamp: Long? = null
    private var vehicleModificationTimeStamp: Long? = null
    private var bayNumber: String? = null
    private var keyHookNumber: String? = null
    private var photoUrls: Map<String, String>? = null
    private var status: String? = null
    private var belongings: Map<String, Any>? = null
    private var ownerContactNumber: String? = null
    private var comments: String? = null
    private var vehicleSortModificationTimeStamp: Long? = null
    private var vehicleNumberDigits: String? = null
    private var vehiclePayment: Int? = null
    private var carRequested: Boolean = false
}